const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const settings = {
  distPath: path.join(__dirname, 'dist'),
  srcPath: path.join(__dirname, 'src'),
};

function srcPathExtend(subpath) {
  return path.join(settings.srcPath, subpath);
}

module.exports = (env, options) => {
  const isDevMode = options.mode === 'development';

  return {
    devtool: isDevMode ? 'source-map' : false,
    resolve: {
      modules: ['node_modules', 'src/js', 'src'],
      extensions: ['.js', '.scss'],
    },
    performance: {
      hints: false,
    },
    module: {
      rules: [
        {
          test: /\.js?$/,
          exclude: /node_modules/,
          enforce: 'pre',
          use: ['babel-loader', 'eslint-loader'],
        },
        {
          test: /\.scss$/,
          exclude: /node_modules/,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                localIdentName: '[name]__[local]--[hash:base64:5]',
                modules: true,
                sourceMap: isDevMode,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: [
                  require('autoprefixer')()
                ],
                sourceMap: isDevMode,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: isDevMode,
              },
            },
          ],
        }, {
          test: /\.(ttf|eot|woff|woff2)$/,
          exclude: /node_modules/,
          use: {
            loader: 'file-loader',
            options: {
              name: 'src/fonts/[name].[ext]',
            },
          },
        }, {
          test: /\.(jpe?g|png|gif|svg|ico)$/i,
          exclude: /node_modules/,
          use: [
            {
              loader: 'file-loader',
              options: {
                outputPath: 'assets/images',
              },
            },
          ],
        },
      ],
    },
    devServer: {
      overlay: true,
      historyApiFallback: true,
    },
    plugins: [
      new CleanWebpackPlugin([settings.distPath], {
        verbose: true,
      }),
      new HtmlWebpackPlugin({
        template: srcPathExtend('index.html'),
      }),
    ],
  };
};
