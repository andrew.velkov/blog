import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, withStyles } from '@material-ui/core';

import { getPosts, createPost, removePost } from 'actions';
import Fetching from 'components/Fetching';
import Button from 'components/Buttons';
import Input from 'components/Form/Input';
import PostItems from 'components/Posts/PostItems';

const styles = {
  wrap: {
    paddingTop: 15,
    maxWidth: 1024,
    margin: 'auto',
  },
  card: {
    maxWidth: 320,
    marginBottom: 15,
    marginRight: 15,
  },
};

class PostsContainer extends Component {
  state = {
    title: '',
    body: '',
    image: '',
    author: 'İlon Mask',
  }

  componentDidMount() {
    const { getPostsData } = this.props;
    getPostsData();
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }

  handleClick = (e) => {
    e.preventDefault();
    const { getPostsData, createPostData } = this.props;
    const {
      title, body, image, author,
    } = this.state;
    const newPostData = {
      title, body, author, image, date: Date.now(),
    };

    if (title !== '') {
      createPostData(newPostData).then((res) => {
        if (res.payload.status === 201) {
          getPostsData();
        }
      });
    }
  }

  handleDelete = (id) => {
    const { getPostsData, removePostData } = this.props;
    removePostData(id).then((res) => {
      if (res.payload.status === 200) {
        getPostsData();
      }
    });
  }

  render() {
    const { getposts: { data, loaded, loading }, classes } = this.props;
    const { title, body, image } = this.state;
    const posts = loaded && data.data;
    const isDisabled = title === '';

    return (
      <section className={classes.wrap}>
        <form>
          <Grid container spacing={24}>
            <Grid item xs={12} sm={4} md={4}>
              <Input
                label="Title"
                name="title"
                value={title}
                onChange={this.handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={4} md={4}>
              <Input
                label="Description"
                name="body"
                value={body}
                multiline
                onChange={this.handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={4} md={4}>
              <Input
                label="Image URL"
                name="image"
                value={image}
                onChange={this.handleChange}
              />
            </Grid>
          </Grid>
          <Button type="submit" disabled={isDisabled} onClick={this.handleClick}>Create Post</Button>
        </form>

        <br />
        <br />

        <Fetching isFetching={loading}>
          <Grid container spacing={24}>
            {(loaded && posts.length > 0) && posts.map(post => (
              <PostItems
                key={post.id}
                post={post}
                className={classes.card}
                isDisabled={isDisabled}
                onClick={() => this.handleDelete(post.id)}
              />
            ))}

            {(loaded && posts.length <= 0) && <div>No results...</div>}
          </Grid>
        </Fetching>
      </section>
    );
  }
}

export default connect(state => ({
  getposts: state.get.getposts,
  getcomments: state.get.getcomments,
}), dispatch => ({
  getPostsData: () => dispatch(getPosts()),
  removePostData: id => dispatch(removePost(id)),
  createPostData: data => dispatch(createPost(data)),
}))(withStyles(styles)(PostsContainer));
