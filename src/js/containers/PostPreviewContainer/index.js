import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';

import { getPosts } from 'actions';
import Fetching from 'components/Fetching';
import PostItem from 'components/Posts/PostItem';

const styles = {
  card: {
    maxWidth: 600,
    margin: 'auto',
    textAlign: 'center',
  },
};

class PostsContainer extends Component {
  componentDidMount() {
    const { getPostsData } = this.props;
    getPostsData();
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }

  render() {
    const { getposts: { data, loaded, loading }, classes, postId } = this.props;
    const post = loaded && data.data.find(item => item.id === +postId);

    return (
      <Fetching isFetching={loading}>
        <PostItem
          post={post}
          className={classes.card}
        />
      </Fetching>
    );
  }
}

export default connect((state, props) => ({
  getposts: state.get.getposts,
  postId: props.match.params.id,
}), dispatch => ({
  getPostsData: () => dispatch(getPosts()),
}))(withStyles(styles)(PostsContainer));
