export const GET_COMMENTS = 'blog/getcomments/LOAD';
export const GET_COMMENTS_SUCCESS = 'blog/getcomments/LOAD_SUCCESS';
export const GET_COMMENTS_ERROR = 'blog/getcomments/LOAD_ERROR';

export const CREATE_COMMENT = 'blog/createcomment/LOAD';
export const CREATE_COMMENT_SUCCESS = 'blog/creatcomment/LOAD_SUCCESS';
export const CREATE_COMMENT_ERROR = 'blog/creatcomment/LOAD_ERROR';
