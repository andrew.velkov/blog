export const GET_POSTS = 'blog/getposts/LOAD';
export const GET_POSTS_SUCCESS = 'blog/getposts/LOAD_SUCCESS';
export const GET_POSTS_ERROR = 'blog/getposts/LOAD_ERROR';

export const EDIT_POST = 'blog/editpost/LOAD';
export const EDIT_POST_SUCCESS = 'blog/editpost/LOAD_SUCCESS';
export const EDIT_POST_ERROR = 'blog/editpost/LOAD_ERROR';

export const CREATE_POST = 'blog/createpost/LOAD';
export const CREATE_POST_SUCCESS = 'blog/createpost/LOAD_SUCCESS';
export const CREATE_POST_ERROR = 'blog/createpost/LOAD_ERROR';

export const REMOVE_POST = 'blog/removepost/LOAD';
export const REMOVE_POST_SUCCESS = 'blog/removepost/LOAD_SUCCESS';
export const REMOVE_POST_ERROR = 'blog/removepost/LOAD_ERROR';
