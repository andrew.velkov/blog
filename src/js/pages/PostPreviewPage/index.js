import React from 'react';
import PostPreviewContainer from 'containers/PostPreviewContainer';

const PostPreviewPage = ({ ...props }) => <PostPreviewContainer {...props} />;

export default PostPreviewPage;
