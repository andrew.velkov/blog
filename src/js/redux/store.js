import { createStore, applyMiddleware, compose } from 'redux';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import reducer from './reducers';

const client = axios.create({
  baseURL: 'https://simple-blog-api.crew.red',
  responseType: 'json',
});

const store = createStore(
  reducer,
  compose(
    applyMiddleware(axiosMiddleware(client)),
    window.devToolsExtension ? window.devToolsExtension() : f => f,
  ),
);

export default store;
