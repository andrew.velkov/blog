const initialState = {
  loaded: false,
  loading: false,
  data: [],
};

export default function blog(params = '') {
  return (state = initialState, action = {}) => {
    switch (action.type) {
      case `blog/${params}/LOAD`:
        return {
          ...state,
          loading: true,
          loaded: false,
        };
      case `blog/${params}/LOAD_SUCCESS`:
        return {
          ...state,
          loading: false,
          loaded: true,
          data: action.payload,
        };
      case `blog/${params}/LOAD_ERROR`:
        return {
          ...state,
          loading: false,
          loaded: false,
        };
      default:
        return state;
    }
  };
}
