import { combineReducers } from 'redux';

import blog from './blog';

const reducers = combineReducers({
  get: combineReducers({
    getposts: blog('getposts'),
    getcomments: blog('getcomments'),
  }),
});

export default reducers;
