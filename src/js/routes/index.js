import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';

import App from 'containers/App';
import PostsPage from 'pages/PostsPage';
import PostPreviewPage from 'pages/PostPreviewPage';
import NotFoundPage from 'pages/NotFoundPage';

const Routes = () => (
  <HashRouter>
    <App>
      <Switch>
        <Route exact path="/" render={props => <PostsPage {...props} />} />
        <Route path="/posts/:id" render={props => <PostPreviewPage {...props} />} />
        <Route component={NotFoundPage} />
      </Switch>
    </App>
  </HashRouter>
);

export default Routes;
