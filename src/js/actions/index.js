import { getComments, createComment } from './comments';
import {
  getPosts, editPost, createPost, removePost,
} from './posts';

export {
  getPosts,
  editPost,
  createPost,
  removePost,
  getComments,
  createComment,
};
