import * as type from 'constants/comments';

export const getComments = (id = 1, comments = 'comments') => (
  {
    types: [type.GET_COMMENTS, type.GET_COMMENTS_SUCCESS, type.GET_COMMENTS_ERROR],
    payload: {
      request: {
        url: `posts/${id}?_embed=${comments}`,
        method: 'GET',
      },
    },
  }
);

export const createComment = data => (
  {
    types: [type.CREATE_COMMENT, type.CREATE_COMMENT_SUCCESS, type.CREATE_COMMENT_ERROR],
    payload: {
      request: {
        url: '/comments',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: data,
      },
    },
  }
);
