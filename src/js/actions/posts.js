import * as type from 'constants/posts';

export const getPosts = () => (
  {
    types: [type.GET_POSTS, type.GET_POSTS_SUCCESS, type.GET_POSTS_ERROR],
    payload: {
      request: {
        url: '/posts',
        method: 'GET',
      },
    },
  }
);

export const editPost = (id, data) => (
  {
    types: [type.EDIT_POST, type.EDIT_POST_SUCCESS, type.EDIT_POST_ERROR],
    payload: {
      request: {
        url: `/posts/${id}`,
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        data,
      },
    },
  }
);

export const createPost = data => (
  {
    types: [type.CREATE_POST, type.CREATE_POST_SUCCESS, type.CREATE_POST_ERROR],
    payload: {
      request: {
        url: '/posts',
        method: 'POST',
        data,
      },
    },
  }
);

export const removePost = id => (
  {
    types: [type.REMOVE_POST, type.REMOVE_POST_SUCCESS, type.REMOVE_POST_ERROR],
    payload: {
      request: {
        url: `/posts/${id}`,
        method: 'DELETE',
      },
    },
  }
);
