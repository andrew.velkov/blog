import React from 'react';
import { TextField, MenuItem } from '@material-ui/core';

const Select = ({
  data, title, name, value, onChange,
}) => (
  <TextField
    label={title}
    name={name}
    select
    fullWidth
    value={value}
    onChange={onChange}
  >
    {data.map(item => (
      <MenuItem key={item.id} value={item.id}>
        {item.value}
      </MenuItem>
    ))}
  </TextField>
);

export default Select;
