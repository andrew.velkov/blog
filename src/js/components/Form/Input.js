import React from 'react';
import { TextField } from '@material-ui/core';

const Input = ({
  label, name, value, onChange, error, ...input
}) => (
  <TextField
    label={label}
    name={name}
    value={value}
    fullWidth
    margin="normal"
    onChange={onChange}
    {...input}
  />
);

export default Input;
