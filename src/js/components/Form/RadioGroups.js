import React from 'react';
import {
  FormControl, FormLabel, RadioGroup, FormControlLabel, Radio,
} from '@material-ui/core';

const RadioGroups = ({
  name, data, title, value, onChange,
}) => (
  <FormControl component="fieldset">
    <FormLabel component="legend">{title}</FormLabel>
    <RadioGroup
      name={name}
      value={value}
      onChange={onChange}
      style={{display: 'inline-block'}}
    >
      {data.map(item => (
        <FormControlLabel
          key={item.id}
          value={item.value}
          label={item.name}
          control={<Radio color="primary" />}
          style={{ paddingRight: '15px' }}
        />
      ))}
    </RadioGroup>
  </FormControl>
);

export default RadioGroups;
