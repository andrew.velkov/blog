import React from 'react';
import Button from '@material-ui/core/Button';

const Buttons = ({
  variant = 'contained', color = 'primary', children, onClick, ...button
}) => (
  <React.Fragment>
    <Button variant={variant} color={color} onClick={onClick} {...button}>
      {children}
    </Button>

    {variant === ''
      && (
        <Button color={color} onClick={onClick} {...button}>
          {children}
        </Button>
      )
    }
  </React.Fragment>
);

export default Buttons;
