import React from 'react';
import { Link } from 'react-router-dom';
import {
  Grid, Card, CardActionArea, CardContent, Typography, withStyles, CardActions,
} from '@material-ui/core';

const styles = {
  card: {
    maxWidth: 600,
  },
};

const PostItems = ({ post, classes }) => (
  <Grid item xs={12} sm={3}>
    <Card className={classes.card}>
      <CardActionArea>
        {post.image && <img src={post.image} alt="" />}
        <CardContent>
          <Typography gutterBottom variant="h6" component="h4">
            {post.title}
          </Typography>
          {post.author
            && (
              <Typography component="p">
                {post.author}
              </Typography>
            )
          }
          <Typography component="p">
            {post.body}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Link to="/">Back to Home Page</Link>
      </CardActions>
    </Card>
  </Grid>
);

export default withStyles(styles)(PostItems);
