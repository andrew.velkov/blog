import React from 'react';
import { Link } from 'react-router-dom';
import {
  Card, CardActionArea, CardContent, Typography, withStyles, CardActions,
} from '@material-ui/core';
import Button from 'components/Buttons';

const styles = {
  card: {
    maxWidth: 320,
    marginBottom: 15,
    marginRight: 15,
  },
};

const PostItems = ({ post, onClick, classes }) => (
  <Card className={classes.card}>
    <Link to={`/posts/${post.id}`}>
      <CardActionArea>
        {post.image && <img src={post.image} alt="" />}
        <CardContent>
          <Typography gutterBottom variant="h6" component="h4">
            {post.title}
          </Typography>
          {post.author
            && (
              <Typography component="p">
                {post.author}
              </Typography>
            )
          }
        </CardContent>
      </CardActionArea>
    </Link>
    <CardActions>
      <Button color="secondary" type="submit" onClick={onClick}>Delete</Button>
    </CardActions>
  </Card>
);

export default withStyles(styles)(PostItems);
